CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Email autocomplete is a module that suggests and autocompletes the domain
whenever your users type in an email address field.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/email_autocomplete

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/search/email_autocomplete


REQUIREMENTS
------------

No special requirements.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------

No configuration is needed.


MAINTAINERS
-----------

Current maintainers:
 * Mahmoud Zayed - https://www.drupal.org/u/mahmoud-zayed
